import React from 'react';
import { View, Text } from 'react-native';

import Entrada from './Entrada';
import Operacao from './Operacao';
import Comando from './Comando';

/*
 * Mostra os numeros da props num1 e num2 que recebe atravez da props do componente Entrada
 * Lista as operações da props operacao e atualiza a operaçao selecionada no picker atravez da props atualizaValor
 * A operação a ser calculada e recebida pelo atributo acao do componente Comando pela props calcular da classe App
*/
const Painel = props => (
	<View>
		<Entrada num1={props.num1} num2={props.num2} atualizaValor={props.atualizaValor} />
		<Operacao operacao={props.operacao} atualizaOperacao={props.atualizaOperacao} />
		<Comando acao={props.calcular} />
	</View>
);

export { Painel };
