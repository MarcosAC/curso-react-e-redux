import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';


// Envia o resultado do calculo atravez de uma props setada no atributo value
export default props => (
	<View>
		<TextInput style={styles.visor} placeholder='Resultado' editable={false} value={props.resultado} />
	</View>
);

const styles = StyleSheet.create({
	visor: {
		height: 100,
		fontSize: 30
	}
});
