import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Image,
  Text,
  StyleSheet,
  TouchableHighlight
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import BarraNavegacao from './BarraNavegacao';

const detalheContato = require('../imgs/detalhe_contato.png');

export default class CenaContatos extends Component {
  render() {
    return (
			<View style={{ backgroundColor: '#FFF', flex: 1 }}>
        <StatusBar 
          //hidden
          backgroundColor='#61BD8C'
        />

        <BarraNavegacao voltar corDeFundo='#61BD8C' />

        <View style={styles.cabecalho}>
          <Image source={detalheContato} />          
          <Text style={styles.txtTitulo}>Contatos</Text>
        </View>

        <View style={styles.detalheContato}>                   
          <Text style={styles.txtContato}>TEL: (11) 1234-1234</Text>
          <Text style={styles.txtContato}>CEL: (11) 91234-1234</Text>
          <Text style={styles.txtContato}>EMAIL: contato@atmconsultoria.com</Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  cabecalho: {
    flexDirection: 'row',
    marginTop: 20   
  },
  txtTitulo: {
    fontSize: 30,
    color: '#61BD8C',
    marginLeft: 10,
    marginTop: 25
  },
  detalheContato: {
    padding: 20,
    marginTop: 20
  },
  txtContato: {    
    fontSize: 18
  }  
});