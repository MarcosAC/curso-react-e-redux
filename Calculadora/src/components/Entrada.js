import React from 'react';
import { StyleSheet, View } from 'react-native';
import Numero from './Numero';

/*
 * Envia o numero do atributo num atravez de uma props 
 * e o nome do campo num1 e num2 atravez do atributo nome
 * para o componente Numero.
*/
export default props => (
	<View style={styles.numeros}>
		<Numero num={props.num1} atualizaValor={props.atualizaValor} nome='num1' />
		<Numero num={props.num2} atualizaValor={props.atualizaValor} nome='num2' />
	</View>
);

const styles = StyleSheet.create({
	numeros: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	}
})
