import React from 'react';
import { AppRegistry, Text, View, Image, TouchableOpacity, Alert } from 'react-native';

//Formatações
const Estilos = {
  principal: {    
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  botao: {
  	backgroundColor: '#538530',
  	paddingVertical: 10,
  	paddingHorizontal: 30,
  	marginTop: 20
  },
  textoBotao: {
  	color: 'white',
  	fontSize: 16,
  	fontWeight: 'bold'
  }
};

const gerarNovaFrase = () => {
  var numeroAleatorio = Math.random();
  numeroAleatorio = Math.floor(numeroAleatorio * 5);

  //frases
  var frases = Array();
  frases[0] = 'A vida me ensinou que chorar alivia, mas sorrir torna tudo mais bonito.';
  frases[1] = 'Nunca troque o que você mais quer na vida pelo que você mais quer no momento, pois os momentos passam e a vida continua.';
  frases[2] = 'Objetivo do dia: Ser uma pessoa melhor, não perfeito, apenas melhor que ontem!';
  frases[3] = 'Humildade não te faz melhor que ninguém, mas te faz diferente de muitos.';
  frases[4] = 'Eu não sou a melhor pessoa do mundo, mas pelo menos não finjo ser quem não sou.';

  var fraseEscolhida = frases[numeroAleatorio];

<<<<<<< HEAD
  Alert.alert('Frase do Dia', fraseEscolhida);
=======
  Alert.alert('Frases do Dia', fraseEscolhida);
>>>>>>> 505ed07319c4bc4a1f4a7fdcca0d98ae2b3c5f22
}

//Criar o componente
const App = () => {
  const { principal, botao, textoBotao } = Estilos;

  return (
    <View style={principal}>
      <Image source={require('./imgs/logo.png')}/>      

      <TouchableOpacity onPress={gerarNovaFrase} style={botao}>        
      	<Text style={textoBotao}>Nova frase</Text>
      </TouchableOpacity>
    </View>
    );
};

//Renderizar para o dispositivo
AppRegistry.registerComponent('FrasesDoDia', () => App);