/*
Classe do componente ListaItens.
Lista uma lista de itens.
*/

import React, { Component } from 'react';
import {
  ScrollView,
  Text
} from 'react-native';
import axios from 'axios';
import Itens from './Itens';

export default class ListaItens extends Component {
  /*
  * Dentro do contructor crio minha propriedade state
  * que tem uma variavel que recebe o array da reposta da requisão HTTP
  */
  constructor(props) {
    super(props);
    this.state = { listaItens: [] }; //Varial que recebe a resposta da requisão.
  }
  componentWillMount() {
    //Requisição HTTP da API os produtos.
    axios.get('http://faus.com.br/recursos/c/dmairr/api/itens.html')

      /*
      * No .then passo a resposta da minha requisição 
      * para a varial listaItens do constructor
      * pelo this.setState.
      */      
      .then(response => { this.setState({ listaItens: response.data }); }) // O .then executa alguma coisa depois da requisão.
      .catch(() => { console.log('Erro ao recuperar os dodos!') }); // .cath caso a requisão falhe retorna o erro.
  }
  render() {
    return (
        /*
        * O método map faz um loop no array de itens
        * e retorna atravez de uma função de call back
        * a informação do array.
        *
        * No componente itens atravez de uma props 'item={}'
        * passo os dados do parametro 'item' da função de call back
        * para o componente itens na classe Itens.
        */
        <ScrollView style={{ backgroundColor: '#DDD' }}>
          { this.state.listaItens.map(item => (<Itens key={ item.titulo } item={ item } />)) }         
        </ScrollView>     
    );
  }
}