import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Image,
  Text,
  StyleSheet  
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import BarraNavegacao from './BarraNavegacao';

const detalheServicos = require('../imgs/detalhe_servico.png');

export default class CenaServicos extends Component {
  render() {
    return (
			<View style={{ backgroundColor: '#FFF', flex: 1 }}>
        <StatusBar 
          //hidden
          backgroundColor='#19D1C8'
        />

        <BarraNavegacao voltar corDeFundo='#19D1C8' />

        <View style={styles.cabecalho}>
          <Image source={detalheServicos} />          
          <Text style={styles.txtTitulo}>Nossos Serviços</Text>
        </View>

        <View style={styles.detalheServicos}>          
          <Text style={styles.txtDetalheServicos}>- Consultoria</Text>           
          <Text style={styles.txtDetalheServicos}>- Processos</Text>
          <Text style={styles.txtDetalheServicos}>- Acompanhamento de Projetos</Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  cabecalho: {
    flexDirection: 'row',
    marginTop: 20   
  },
  txtTitulo: {
    fontSize: 30,
    color: '#EC7148',
    marginLeft: 10,
    marginTop: 25
  },
  detalheServicos: {
    padding: 20,
    marginTop: 10
  },
  txtDetalheServicos: {
    fontSize: 18   
  }
});
