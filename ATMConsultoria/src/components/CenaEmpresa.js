import React, { Component } from 'react';
import {
    View,
    StatusBar,
    Image,
    Text,
    StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import BarraNavegacao from './BarraNavegacao';

const detalheEmpresa = require('../imgs/detalhe_empresa.png');

export default class CenaEmpresa extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#FFF', flex: 1 }}>
                <StatusBar
                    //hidden
                    backgroundColor='#EC7148'
                />

                <BarraNavegacao voltar corDeFundo='#EC7148' />

                <View style={styles.cabecalho}>
                    <Image source={detalheEmpresa} />
                    <Text style={styles.txtTitulo}>A Empresa</Text>
                </View>

                <View style={styles.detalheEmpresa}>
                    <Text style={styles.txtDetalheEmpresa}>Lorem ipsum dolorem Lorem ipsum dolorem Lorem ipsum dolorem</Text>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    cabecalho: {
        flexDirection: 'row',
        marginTop: 20
    },
    txtTitulo: {
        fontSize: 30,
        color: '#EC7148',
        marginLeft: 10,
        marginTop: 25
    },
    detalheEmpresa: {
        padding: 20,
        marginTop: 10
    },
    txtDetalheEmpresa: {
        fontSize: 18
    }
});
