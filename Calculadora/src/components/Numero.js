import React from 'react';
import { TextInput, StyleSheet } from 'react-native';

/*
 * Envia o nome do campo e o valor dos campos atravez do onChangeText
 * para o método atualizaValor da classe App.
*/
export default props => (
	<TextInput style={styles.numero} value={props.num} onChangeText={valorCampo => props.atualizaValor(props.nome, valorCampo)}/>
);

const styles = StyleSheet.create({
	numero: {
		width: 140,
		height: 80,
		fontSize: 20
	}
});
