import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';

//Importa meu componente ListaDeItens
import ListaItens from './src/components/ListaItens';

//const instructions = Platform.select({
  //ios: 'Press Cmd+R to reload,\n' +
    //'Cmd+D or shake for dev menu',
  //android: 'Double tap R on your keyboard to reload,\n' +
    //'Shake or press menu button for dev menu',
//});

export default class ListaDeProduto extends Component {
  render() {
    return (
      <ListaItens />
    );
  }
}

// Registra a aplicação.
//AppRegistry.registerComponent('ListaDeProduto', () => ListaDeProduto);
