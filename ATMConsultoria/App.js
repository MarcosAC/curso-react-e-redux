import React, { Component } from 'react';

// Navegação entre paginas utilizando 'react-native-router-flux
import {  
  Router,
  Scene
} from 'react-native-router-flux';

// Componentes customizados
import BarraNavegacao from './src/components/BarraNavegacao';
import CenaPrincipal from './src/components/CenaPrincipal';
import CenaClientes from './src/components/CenaClientes';
import CenaContatos from './src/components/CenaContatos';
import CenaEmpresa from './src/components/CenaEmpresa';
import CenaServicos from './src/components/CenaServicos';

export default class App extends Component {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="principal"
            component={CenaPrincipal}
            navBar={BarraNavegacao}
            initial
          />

          <Scene key="clientes"
            component={CenaClientes}
            hideNavBar={true} 
          />

          <Scene key="contatos"
            component={CenaContatos}
            hideNavBar={true} 
          />

          <Scene key="empresa"
            component={CenaEmpresa}
            hideNavBar={true} 
          />

          <Scene key="servicos"
            component={CenaServicos}
            hideNavBar={true} 
          />
        </Scene>
      </Router>      
    );        
  }
}