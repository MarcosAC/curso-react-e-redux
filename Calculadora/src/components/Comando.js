import React from 'react';
import { Button } from 'react-native'

// Recebe a ação atravez da props calcular do componente Painel
export default props => (
	<Button title="Calcular" onPress={props.acao} />
);
