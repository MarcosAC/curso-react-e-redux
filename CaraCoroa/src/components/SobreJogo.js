import React, { Component } from 'react';
import { Text } from 'react-native';

export default class SobreJogo extends Component {
  render() {
    return (
      <Text style={{ flex: 1, backgroundColor: '#61BD8C' }}>
        Jogo Cara ou Coroa. Projeto de exercicio do Curso Desenvolvimento para Andoid e IOS com React e Redux.
      </Text>
    );
  }
}
